Feature: Create Lead
Background:
Given Open the Browser
And Maximize the Browser
And Set the Timeout
And Launch the URL
Scenario: Verify Create Lead
And Enter the Username as DemoSalesManager
And Enter the Password as crmsfa
When Click the Login Button
Then Verify the Login
And Click the CRM/SFA Lnk
And Click the Leads
And Click the Create Leads Section
And Enter the Company Name as CTS
And Enter the Firstname as Mithil
And Enter the Lastname as Kumar
When Click Create Lead Button
Then Verify the Create Lead
 
Scenario Outline: Create Lead
And Enter the Username as <username> 
And Enter the Password as <password>
When Click the Login Button
Then Verify the Login
And Click the CRM/SFA Lnk
And Click the Leads
And Click the Create Leads Section
And Enter the Company Name as <cname>
And Enter the Firstname as <fname>
And Enter the Lastname as <lname>
When Click Create Lead Button
Then Verify the Create Lead

Examples:
|username|password|cname|fname|lname|
|DemoSalesManager|crmsfa|CTS|Uma|S|
|DemoCSR|crmsfa|CTS|Nilo|Fernisha|
